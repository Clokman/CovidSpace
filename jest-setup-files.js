// Polyfills etc. /////////////////////////////////////////////////////////////////////////////

require( 'jest-canvas-mock' )

// Import polyfill for fetch() method
if( typeof fetch !== 'function' ) {
    global.fetch = require( 'node-fetch-polyfill' )
}

// Import polyfill for Object.fromEntries() method
if( typeof Object.fromEntries !== 'function' ) {
    Object.fromEntries = require( 'object.fromentries' )
}


// Testing Library ////////////////////////////////////////////////////////////////////////////////////////////////
require( '@testing-library/jest-dom/dist/extend-expect' )
require( '@testing-library/dom' )
require( '@testing-library/jest-dom' )


// JQuery and JQuery-UI ////////////////////////////////////////////////////////////////////////////////////////////////
global.$ = global.jQuery = require( './designer/js/external/jquery-3.6.0' )
require( 'jquery-ui-dist/jquery-ui' )


// JestUtils //////////////////////////////////////////////////////////////////////////////////////////////
const jestConsole = require( './designer/js/external/jest-utils-1.0/jest-console' )
global.expectTable = jestConsole.expectTable
global.expectConsoleHistory = jestConsole.expectConsoleHistory
global.clearConsoleHistory = jestConsole.clearConsoleHistory

const jestDom = require( './designer/js/external/jest-utils-1.0/jest-dom' )
global.initializeDomWithSvg = jestDom.initializeDomWithSvg
global.writeDomToFile = jestDom.writeDomToFile


// Modules Being Tested /////////////////////////////////////////////////////////////////////////////////////////////
global.spaceDesigner = require( './designer/js/space-designer' )
