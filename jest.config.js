module.exports = {

    'setupFilesAfterEnv': [ './jest-setup-files.js' ],
    // "preset": "jest-puppeteer",
    "testEnvironment": 'jsdom'
    // "runner": "groups"

}


// Use Jest Projects to run both JSDom and Node tests in the same project
// module.exports = {
//     projects: [
//         {
//             displayName: 'dom',
//             testEnvironment: 'jsdom',
//             testMatch: ['**/__tests__/**/*.test.js?(x)']
//         },
//         {
//             displayName: 'node',
//             testEnvironment: 'node',
//             testMatch: [
//                 '**/__tests__/**/*.test.node.js?(x)',
//             ]
//         },
//     ],
// };