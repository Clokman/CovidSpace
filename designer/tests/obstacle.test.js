//// UNIT TESTS ////////////////////////////////////////////////////////////////////////////////////////////////////////

const { waitFor } = require( '@testing-library/dom' )
const { configure } = require( '@testing-library/dom' )
configure(
    {
        asyncUtilTimeout: 2,
        testIdAttribute: 'id'
    }
)

//// Create Obstacle ///////////////////////////////////////////////////////////////

describe( 'Create Obstacle', () => {

    // Create the venue
    document.body.innerHTML =
        `<div id="venue" 
                class="container ui-widget-content resizable"
                style="width:640px; height:480px; background-color:lightgray" 
        ></div>`


    test( 'Create an Obstacle at default coordinates', async () => {

        // Obstacle should be created correctly
        const myObstacle = new spaceDesigner.Obstacle()
        expect( myObstacle ).toBeDefined()

        // Obstacle should have correct properties
        const $obstacle0 = document.querySelector( '#obstacle-0' )
        const left = window.getComputedStyle( $obstacle0 ).getPropertyValue( 'left' )
            , top = window.getComputedStyle( $obstacle0 ).getPropertyValue( 'top' )
            , parentId = $obstacle0.parentNode.id
        expect( left ).toBe( '12px' )
        expect( top ).toBe( '15px' )
        expect( parentId ).toBe( 'venue' )

        // writeDomToFile( '/Users/clokman/Code/CovidSpace/designer/tests/dom-out/1.html' )

    } )


    test( 'Create an Obstacle at specific coordinates', async () => {

        // Obstacle should be created correctly
        const myObstacle = new spaceDesigner.Obstacle( '150px', '150px' )
        expect( myObstacle ).toBeDefined()

        // Obstacle should have correct properties
        const $obstacle0 = document.querySelector( '#obstacle-1' )
        const left = window.getComputedStyle( $obstacle0 ).getPropertyValue( 'left' )
            , top = window.getComputedStyle( $obstacle0 ).getPropertyValue( 'top' )
            , parentId = $obstacle0.parentNode.id
        expect( left ).toBe( '150px' )
        expect( top ).toBe( '150px' )
        expect( parentId ).toBe( 'venue' )

    } )

} )




//// Export Layout as CSV ///////////////////////////////////////////////////////////////

describe( 'Export Layout as CSV', () => {

    // TODO: Download should actually be tested
    test( 'Should download CSV', () => {

        // Obstacle should be created correctly
        const myObstacle = new spaceDesigner.Obstacle()
        spaceDesigner.Layout.export()

    } )

} )



//// Helper Functions ///////////////////////////////////////////////////////////////

describe( 'Helper Functions', () => {


    test( 'Should find midpoint from clicked location', () => {

        const clickedX = 100
            , clickedY = 100

        const {centerX, centerY} = spaceDesigner.Obstacle.findCenterPointFromClickedCoordinates( clickedX, clickedY )
        expect( centerX ).toBe(75)
        expect( centerY ).toBe(75)

    } )


} )

