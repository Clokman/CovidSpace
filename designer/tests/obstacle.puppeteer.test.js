//// INTEGRATION TESTS /////////////////////////////////////////////////////////////////////////////
const puppeteer = require( 'puppeteer' )
// require ('@testing-library/dom')
// require ('@testing-library/jest-dom')
// require('pptr-testing-library/extend')


describe( 'Create new obstacles', () => {

    test( 'Create new obstacle with double click',

        async () => {

            let browser = await puppeteer.launch( {
                headless: true
                // headless: false
            } )

            let page = await browser.newPage()

            const xCoordinate = 200
            const yCoordinate = 200

            // Crate new obstacle via button click
            await page.goto( 'http://localhost:3000/CovidSpace/designer' )
            await page.click( '#new-obstacle' )
            await page.waitForSelector( '#obstacle-0' )

            // Create new obstacle via double click
            await page.mouse.click( xCoordinate, yCoordinate, { clickCount: 2 } )
            await page.waitForSelector( '#obstacle-1' )

            const style = await page.evaluate( () => {

                const obstacle1_element = document.querySelector( '#obstacle-1' )

                const left = getComputedStyle(obstacle1_element).getPropertyValue('left')
                const top = getComputedStyle(obstacle1_element).getPropertyValue('top')

                return [left, top]
            } )

            // Expect obstacle at double click coordinates
            await expect(style).toEqual(['175px', '175px'])

            browser.close()

        }, 6000
    )
} )


