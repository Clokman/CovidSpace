//// UMD HEAD ////////////////////////////////////////////////////////////////////////
// UMD head and foot patterns adapted from d3.js
( function ( global, factory ) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory( exports ) :
        typeof define === 'function' && define.amd ? define( [ 'exports' ], factory ) :
            ( factory( ( global.spaceDesigner = global.spaceDesigner || {} ) ) )
}( this, ( function ( exports ) {
    'use strict'
//////////////////////////////////////////////////////////////////////////////////////
    // Defaults
    const defaultObstacleStyle = {
        left: '12px',
        top: '15px',
        width: '50px',
        height: '50px'
    }

    /**
     * Initializes (or re-initializes) jQuery UI so that correct jQuery UI scripts are bound to relevant HTML
     * elements (e.g., if an element is added the 'draggable' class, jQuery UI should be reinitialized, so that the
     * JS function (in jQuery UI's library) that allows dragging elements can be bound to the
     * elements with this class name.).
     */
    function jqueryUiInit() {
        $( function () {
            $( '.draggable' ).draggable()
            $( '.resizable' ).resizable()
        } )
    }




    /**
     * Handles exporting obstacle positions
     */
    class Layout {

        constructor() {}


        static export() {

            // Get CSS style data for each obstacle as an array of strings
            const allObstacles = document.querySelectorAll( '.obstacle' )
            const styles = []
            allObstacles.forEach( ( obstacle ) => {
                const style = obstacle.getAttribute( 'style' )
                styles.push( style )
            } )

            // Create the CSV content from CSS style data
            const headerRow = [ 'left', 'top', 'width', 'height' ]
                , data = styles
            const csvContent = this._formatDataAsCsv( data, headerRow )

            // Download CSV
            const content = csvContent
                , filename = 'spaceDesignerLayout.csv'
            Layout._downloadCSV( content, filename )
            console.log( csvContent )

        }


        static _formatDataAsCsv( data, headerRow ) {

            // Make a dataset out of the data by transforming the data to an array of arrays
            const dataset = [ headerRow ] // start the dataset with the header row
            data.forEach( row => {
                const rowWithoutLetters = row.replace( /[a-z,: ]/g, '' )
                const rowAsStringsArray = rowWithoutLetters.split( ';' )
                const rowAsNumbersArray = rowAsStringsArray.map( ( propertyString ) => Number( propertyString ) )
                dataset.push( rowAsNumbersArray )
            } )

            // Convert dataset to a CSV-compatible string
            const csvContent = dataset.map( row => row.join( ',' ) ).join( '\n' )
            return csvContent

        }


        static _downloadCSV( content, filename = 'export.csv' ) {

            if( content == null ) return

            const blob = new Blob( [ content ] )

            if( window.navigator.msSaveOrOpenBlob )  // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
                window.navigator.msSaveBlob( blob, args.filename )
            else {
                const a = window.document.createElement( 'a' )
                a.href = window.URL.createObjectURL( blob, { type: 'text/plain' } )
                a.download = filename
                document.body.appendChild( a )
                a.click()  // IE: "Access is denied"; see:
                // https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
                document.body.removeChild( a )
            }

        }

    }




    let obstacleCounter = 0

    // TODO: Double-clicking should create an obstacle at clicked location.
    // TODO: It should not be possible to drag objects out of the venue area
    /**
     * Creates and manages obstacles
     */
    class Obstacle {

        constructor( left = defaultObstacleStyle.left, top = defaultObstacleStyle.top ) {

            // Assign CSS style parameters
            const styleParameters =
                `left: ${left}; ` +
                `top: ${top}; ` +
                `width: ${defaultObstacleStyle.width}; ` +
                `height: ${defaultObstacleStyle.height}`

            // Create a new obstacle
            $( '#venue' ).append(
                '<div ' +
                `id = \'obstacle-${obstacleCounter}\' ` +
                `class=\'obstacle ui-widget-content draggable resizable\' ` +
                `style=\'${styleParameters}\' >` +
                '</div>'
            )

            obstacleCounter++

            // Reinitialize jQuery UI
            jqueryUiInit()
        }


        static findCenterPointFromClickedCoordinates( clickedX, clickedY ) {

            // Get width and height values as number
            const widthString = spaceDesigner.defaultObstacleStyle.width
                , heightString = spaceDesigner.defaultObstacleStyle.height
            const width = Number( widthString.replace( 'px', '' ) )
                , height = Number( heightString.replace( 'px', '' ) )

            // Calculate midpoint of the rectangle
            const centerX = clickedX - width / 2
                , centerY = clickedY - height / 2

            return { centerX, centerY }
        }

    }




//// UMD FOOT ////////////////////////////////////////////////////////////////////////

    //// MODULE.EXPORTS ////
    exports.Obstacle = Obstacle
    exports.jqueryUiInit = jqueryUiInit
    exports.Layout = Layout
    exports.defaultObstacleStyle = defaultObstacleStyle

    Object.defineProperty( exports, '__esModule', { value: true } )

} ) ) )
//////////////////////////////////////////////////////////////////////////////////////





